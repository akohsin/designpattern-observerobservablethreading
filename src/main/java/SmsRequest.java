public class SmsRequest implements Runnable {
    private Message msg;
    private ISmsSender smsStation;

    public SmsRequest(Message msg, ISmsSender smsStation) {
        this.msg = msg;
        this.smsStation = smsStation;
    }

    public void run() {
        try {
            System.out.println("Rozpoczynam szyfrowanie wiadomości" + msg.getNumer());
            Thread.sleep(100*msg.getTresc().length());
            System.out.println("wysyłam wiadomość");
            smsStation.sendSms(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
