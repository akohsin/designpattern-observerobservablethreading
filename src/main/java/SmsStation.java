import java.util.Observable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


class SmsStation extends Observable implements ISmsSender {

    private Executor antennas = Executors.newFixedThreadPool(5);
    //    private Executor workers = Executors.newFixedThreadPool(5);
//
//    public void addNewOrder(){
//        workers.execute(new Order());
//        System.out.println("new Order arrived");
//    }


    public void addPhone(Phone p) {
        this.addObserver(p);
    }

    public void sendRequest(String numer, String tresc) {
        antennas.execute(new SmsRequest(new Message(numer, tresc), this));

    }
    public void sendSms(Message msg){
        setChanged();
        notifyObservers(msg);
    }
}
